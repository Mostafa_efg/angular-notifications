// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

/**
 * copy and paste your firebase config in firebase console
 * Authentication > Web Setub
 */
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB9Q6uo8MufZw7XnPPaHWyjCqDWzIJIBA0",
    authDomain: "elcartona-onlineshopping.firebaseapp.com",
    databaseURL: "https://elcartona-onlineshopping.firebaseio.com",
    projectId: "elcartona-onlineshopping",
    storageBucket: "elcartona-onlineshopping.appspot.com",
    messagingSenderId: "99563296075",
    appId: "1:99563296075:web:bb1c5807c91b0afdf944ce",
    measurementId: "G-9F6Q64BNW9"
  }
};
