// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/7.2.1/firebase-analytics.js');


{
    /* <script src="https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.2.1/firebase-analytics.js"></script> */
}


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    'messagingSenderId': '99563296075'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();