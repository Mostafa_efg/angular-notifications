import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee.model';
import { EmployeeService } from '../services/employees.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  public employeesList: Employee[];

  constructor(private employeeService: EmployeeService) {

    this.employeesList = new Array<Employee>();
  }

  ngOnInit() {

    this.employeeService.getEmployees().subscribe((employees: Employee[]) => {

      if (employees.length > 0) {

        this.employeesList = employees;
      }

    })

  }

}
