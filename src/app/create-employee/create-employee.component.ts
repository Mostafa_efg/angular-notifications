import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee.model';
import { EmployeeService } from '../services/employees.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  public employeeModel: Employee;

  constructor(private empployeeService: EmployeeService, private _router: Router, ) {

    this.employeeModel = new Employee();

  }

  ngOnInit() {
  }

  onAddEmployee() {

    this.empployeeService.addEmployee(this.employeeModel).subscribe((response: any) => {

      if (response != null) {

        this._router.navigate(['list']);
      }

    })

  }

}
